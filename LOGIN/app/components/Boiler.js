import React, { Component } from "react";
import { AppRegistry, StyleSheet, Text, View } from "react-native";
import { StackNavigator } from "react-navigation";
export default class Boiler extends Component {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: "#777",
      elevation: null
    },
    headerLeft: null
  };
  render() {
    return <Text style={styles.text}>
    Bem Vindo!
    </Text>;
  }
}

const styles = StyleSheet.create({
 
  text: {
    textAlign: "center",
    color: "#FFF",
    fontWeight: "700",
    fontFamily: "monospace"
  },
});


AppRegistry.registerComponent("Boiler", () => Boiler);
